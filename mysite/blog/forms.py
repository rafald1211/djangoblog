from django import forms
from .models import Comment

class NewPost(forms.Form):
   title = forms.CharField(max_length=100)
   name = forms.CharField(max_length=25)
   body = forms.CharField(required=False, max_length=200)

class EmailPostForm(forms.Form):
   name = forms.CharField(max_length=25)
   email = forms.EmailField()
   to = forms.EmailField()
   comments = forms.CharField(required=False,widget=forms.Textarea)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('name', 'email', 'body')

class SearchForm(forms.Form):
	query = forms.CharField()